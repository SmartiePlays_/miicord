# miicord
A discord bot meant for the Miicord server

Miicord is a Discord server that mixes together the magic of Discord, and the social aspects of Miiverse. This bot helps Miicord feel a lot more like Miiverse, as it turns messages into Miiverse-like embeds.

Written in discord.js
